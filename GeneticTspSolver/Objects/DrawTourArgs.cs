﻿using System.Collections.Generic;

namespace GeneticTspSolver.Objects
{
    public class DrawTourArgs
    {
        public Tour TourToDraw { get; private set; }
        public long StopWatchMillis { get; private set; }
        public List<string> PopulationDescriptions { get; private set; }

        public DrawTourArgs(Tour tour, long millis, List<string> populationDescriptions)
        {
            //
            // We're running in parallel. Make sure to send a copy and not a reference.
            // Shit can get crazy out there.
            //
            TourToDraw = new Tour(tour);
            StopWatchMillis = millis;
            PopulationDescriptions = populationDescriptions;
        }
    }
}
