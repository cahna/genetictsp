﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace GeneticTspSolver.Objects
{
    public class ParallelGeneticTsp : GeneticTsp
    {
        #region Class Variables

        /// <summary>
        ///     
        /// </summary>
        private readonly CsvBuilder Csv;

        #endregion // Class Variables

        #region Constructor

        public ParallelGeneticTsp(string infile, int seed = -1) : base(infile, seed)
        {
            // Start csv builder
            Csv = new CsvBuilder(AlgorithmSettings.GenerateRunOutputFilename(), new List<object> { "Generations", "Shortest Tour", "Range", "Average" });
        }

        public ParallelGeneticTsp(string infile) : this(infile, -1)
        { }

        #endregion // Constructor

        #region Public Methods

        /// <summary>
        ///     Method to be used as a background worker to allow new generations
        ///     to be created non-stop as a background process. Alerts the control
        ///     thread if a new shortest tour has been found with ReportProgress
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Arguments sent to method when invoked</param>
        public void Genetic_DoWork(object sender, DoWorkEventArgs e)
        {
            //
            // Prepare background worker
            //
            var worker = sender as BackgroundWorker;

            if (worker == null)
                throw new Exception("Worker is null");

            //
            // Immediately report progress if this is the first generation
            //
            if (Generations <= 0)
            {
                var populationDescription = new List<string>(100);
                for (var i = 0; i < 100; i++)
                    populationDescription.Add(Population[i].ToString());

                worker.ReportProgress(Generations,
                                      new DrawTourArgs(Population.First(), RunTimeWatch.ElapsedMilliseconds,
                                                       populationDescription));
            }

            //
            // Loop indefinitely until cancel (stop) requested
            //
            while (!worker.CancellationPending)
            {
                //
                // NextGeneration will return true if a new shortest tour has been found
                //
                if (NextGeneration())
                {
                    var populationDescription = new List<string>(100);
                    for (var i = 0; i < 100; i++)
                        populationDescription.Add(Population[i].ToString());

                    worker.ReportProgress(Generations, new DrawTourArgs(Population.First(), RunTimeWatch.ElapsedMilliseconds, populationDescription));
                }

                //
                // Calculate population statistics
                //
                var distanceRange = Population.Max().RouteDistance - Population.Min().RouteDistance;

                double distanceTotal = 0;
                distanceTotal += Population.Sum(t => t.RouteDistance);

                var distanceAverage = distanceTotal / Population.Count;

                Csv.AddRow(new List<object> { Generations, Population.First().RouteDistance, distanceRange, distanceAverage });
            }

            if(worker.CancellationPending)
                Csv.CancelWorker();
        }

        #endregion Public Methods
    }
}
