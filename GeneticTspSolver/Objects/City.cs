﻿using System.Diagnostics;

namespace GeneticTspSolver.Objects
{
    [DebuggerStepThrough]
    public class City
    {
        #region Private Class Variables

        #endregion // Private Class Variables

        #region Public Accessors

        public int Index { get; private set; }

        public double X { get; private set; }

        public double Y { get; private set; }

        #endregion // Public Accessors

        #region Public Methods

        #endregion // Public Methods

        #region Constructors

        /// <summary>
        ///     Create a new node and define all its properties
        /// </summary>
        /// <param name="nodeIndex">City number (ID). Should be value parsed from TSP file</param>
        /// <param name="nodeX">X Coordinate</param>
        /// <param name="nodeY">Y Coordinate</param>
        public City(int nodeIndex, double nodeX, double nodeY)
        {
            Index = nodeIndex - 1;
            X = nodeX;
            Y = nodeY;
        }

        /// <summary>
        ///     Create a node without a valid ID
        /// </summary>
        /// <param name="x">X Coordinate</param>
        /// <param name="y">Y Coordinate</param>
        public City(double x, double y) : this(-1, x, y)
        { }

        /// <summary>
        ///     Create a new duplicate of a node
        /// </summary>
        /// <param name="c">City to create a copy of</param>
        public City(City c)
        {
            Index = c.Index;
            X = c.X;
            Y = c.Y;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Dot product
        /// </summary>
        /// <param name="a">First City</param>
        /// <param name="b">Second City</param>
        /// <returns>Dot product : (x1*x2) + (x1*y2)</returns>
        public static double operator *(City a, City b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        /// <summary>
        ///     Cross product
        /// </summary>
        /// <param name="a">First City</param>
        /// <param name="b">Second City</param>
        /// <returns>Cross Product : (x1*y2) - (y1*x2)</returns>
        public static double operator ^(City a, City b)
        {
            return a.X * b.Y - a.Y * b.X;
        }

        /// <summary>
        ///     Vector addition
        /// </summary>
        /// <param name="a">First City</param>
        /// <param name="b">Second City</param>
        /// <returns>New city with x,y values from a and b summed</returns>
        public static City operator +(City a, City b)
        {
            return new City(a.X + b.X, a.Y + b.Y);
        }

        /// <summary>
        ///     Vector Subtraction
        /// </summary>
        /// <param name="a">First City</param>
        /// <param name="b">Second City</param>
        /// <returns>New city with x,y values from a and b subtracted in the order of (a - b)</returns>
        public static City operator -(City a, City b)
        {
            return new City(a.X - b.X, a.Y - b.Y);
        }

        /// <summary>
        ///     String representation of this city's data including Index, X, and Y values
        /// </summary>
        /// <returns>A tring representation of this city's data</returns>
        public override string ToString()
        {
            return string.Format("{0:000}: {1:00.00} | {2:00.00}", Index, X, Y);
        }

        #endregion
    }
}