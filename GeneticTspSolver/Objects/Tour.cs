﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneticTspSolver.Objects
{
    public class Tour : IComparable<Tour>, IEquatable<Tour>
    {
        /// <summary>
        ///     How to order the tour upon instantiation
        /// </summary>
        public enum Connection
        {
            InOrder,
            ClosestWithRandom,
            RandomOnly,
            ClosestCity,
            GreedyPointToLine
        }

        #region Class Variables

        /// <summary>
        ///     The Hamiltonian cycle distance for the route
        /// </summary>
        public double RouteDistance { get; private set; }

        /// <summary>
        ///     Order with which to visit the cities
        /// </summary>
        public List<int> Route { get; private set; }

        #endregion // Class Variables

        #region Constructor(s)

        /// <summary>
        ///     Creates a new Tour object and associated route based upon the requested connection method
        /// </summary>
        /// <param name="cityCount">How many cities are in the Map</param>
        /// <param name="method">Initial connection method for the Hamiltonian Cycle</param>
        /// <exception cref="NotImplementedException">Must create a tour with 3 or more cities.</exception>
        public Tour(int cityCount, Connection method = Connection.InOrder)
        {
            //
            // Quick sanity check. We're not implementing a solver for less than 3 cities.
            //
            if (cityCount < 3)
                throw new Exception("Use a map with 3 or more cities!");

            Route = new List<int>(cityCount);

            // Create the list of available cities
            var cities = new List<int>();
            for (var i = 0; i < cityCount; i++)
                cities.Insert(i, i);

            switch (method)
            {
                case Connection.RandomOnly:
                    _initializeRandom(cities);
                    break;

                case Connection.ClosestWithRandom:
                    _initializeClosestWithRandom(cities);
                    break;

                case Connection.ClosestCity:
                    _initializeClosestCity(cities);
                    break;
                case Connection.GreedyPointToLine:
                    _initializeGreedyPointToLine(cities);
                    break;
                default:
                    Route = cities;
                    break;
            }

            // Set route distance
            RouteDistance = CalculateDistance();
        }

        public Tour(List<int> route)
        {
            Route = new List<int>(route.Count);
            foreach(var r in route)
                Route.Add(r);

            // Set route distance
            RouteDistance = CalculateDistance();
        }

        public Tour(Tour tour)
        {
            // Copy members into new tour
            Route = new List<int>(tour.Route.Count);
            foreach (var r in tour.Route)
                Route.Add(r);

            // Set route distance directly. No need to recalculate
            RouteDistance = tour.RouteDistance;
        }

        #endregion // Constructor(s)

        #region Private Methods

        #region Static
        /// <summary>
        ///     Finds the index of the closest available city to the currentCity, or a random index
        /// </summary>
        /// <param name="availableCities">Cities yet to be added to the tour</param>
        /// <param name="currentCity">Most recently added City to the Cycle</param>
        /// <returns>Index within availableCities to next add to the tour</returns>
        private static int ClosestOrRandom(IList<int> availableCities, int currentCity)
        {
            // ReSharper disable RedundantIfElseBlock
            if (GeneticTsp.R.NextDouble() > AlgorithmSettings.UseClosestCityRate)
                return GeneticTsp.R.Next(availableCities.Count);
            else
                return availableCities.IndexOf(ClosestStillRemaining(availableCities, currentCity));
            // ReSharper restore RedundantIfElseBlock
        }

        /// <summary>
        ///     Finds the index of the city closest to currentCity within availableCities
        /// </summary>
        /// <param name="availableCities">Cities yet to be added to the tour</param>
        /// <param name="currentCity">Most recently added City to the Cycle</param>
        /// <returns>Index within availableCities of the closest city to currentCity</returns>
        private static int ClosestStillRemaining(IEnumerable<int> availableCities, int currentCity)
        {
            var closestDistance = 0.0;
            var closestCity = -1;

            foreach (var c in availableCities)
            {
                var dist = Map.DistanceBetweenCities(currentCity, c);
                if (dist < closestDistance || closestCity == -1)
                {
                    closestDistance = dist;
                    closestCity = c;
                }
            }

            return closestCity;
        }

        #endregion // Static

        #region Initial Connections

        private void _initializeRandom(List<int> cities)
        {
            var cityCount = cities.Count;

            // Fill the tour route randomly
            for (var i = 0; i < cityCount; i++)
            {
                var randomIndex = GeneticTsp.R.Next(cities.Count);
                Route.Add(cities[randomIndex]);
                cities.RemoveAt(randomIndex);
            }
        }

        public void _initializeClosestWithRandom(List<int> cities)
        {
            var cityCount = cities.Count;

            // Choose a random city to be the tour start
            var index = GeneticTsp.R.Next(cities.Count);
            var lastCity = cities[index];
            Route.Add(lastCity);
            cities.RemoveAt(index);

            // Fill the rest of the tour with Closest or Random next city
            for (var i = 0; i < cityCount - 1; i++)
            {
                var nextIndex = ClosestOrRandom(cities, lastCity);
                Route.Add(cities[nextIndex]);
                lastCity = cities[nextIndex];
                cities.RemoveAt(nextIndex);
            }
        }

        private void _initializeClosestCity(List<int> cities)
        {
            var cityCount = cities.Count;

            // Choose a random city to be the tour start
            var index = GeneticTsp.R.Next(cities.Count);
            var lastCity = cities[index];
            Route.Add(lastCity);
            cities.RemoveAt(index);

            for (var i = 0; i < cityCount - 1; i++)
            {
                var nextIndex = cities.IndexOf(ClosestStillRemaining(cities, lastCity));
                Route.Add(cities[nextIndex]);
                lastCity = cities[nextIndex];
                cities.RemoveAt(nextIndex);
            }
        }

        private void _initializeGreedyPointToLine(List<int> cities)
        {
            var cityCount = cities.Count;
            var route = new LinkedList<int>();

            //
            // Choose 3 random cities to make the initial loop
            //
            for (var i = 0; i < 3; i++)
            {
                var randomIndex = GeneticTsp.R.Next(cities.Count);
                route.AddLast(cities[randomIndex]);
                cities.RemoveAt(randomIndex);
            }

            //
            // If there's only 3 cities, return. The tour route is optimal.
            //
            if (cityCount == 3) return;

            //
            // Use distance from point to line segment greedy insertion on remaining cities.
            //
            while (cities.Count > 0)
            {
                //
                // Grab a random remaining City
                //
                var cityToAdd = cities[GeneticTsp.R.Next(cities.Count)];

                //
                // Retrieve the distances from the city to be added to all available edges
                //
                var distancesFromEdges = Map.GetDistanceFromAllAvailableEdges(route, cityToAdd);

                //
                // Insert the city AFTER the vertex city that corresponds to the line segment that the
                // city to be added is closest to
                //
                var cityToInsertAfter = distancesFromEdges.First().Value;
                var nodeToInsertAfter = route.Find(cityToInsertAfter);
                if(nodeToInsertAfter != null)
                    route.AddAfter(nodeToInsertAfter, cityToAdd);
                else
                    throw new Exception("route linked list doesn't contain " + cityToInsertAfter);

                cities.Remove(cityToAdd);
            }

            Route = route.ToList();
        }

        #endregion // Initial Connections

        public void UpdateDistance()
        {
            RouteDistance = CalculateDistance();
        }

        /// <summary>
        ///     Traverses all cities and sums their connection distances, including a return
        ///     trip from the last city back to the first
        /// </summary>
        /// <returns>Hamiltonian Cycle Distance of this tour</returns>
        public double CalculateDistance()
        {
            var dist = 0.0;

            // Sum up distances between nodes
            for (var i = 0; i < Route.Count - 2; i++)
            {
                dist += Map.DistanceBetweenCities(Route[i], Route[i + 1]);
            }

            // Include the link back to the original
            dist += Map.DistanceBetweenCities(Route[Route.Count - 1], Route[0]);

            return dist;
        }

        #endregion // Private Methods

        #region Public Methods

        #region Genetic Methods

        /// <summary>
        /// 
        ///     Greedy Crossover based off citation from Sushil J. Louis:
        /// 
        ///         "Greedy crossover selects the first city of one parent, compares the cities 
        ///         leaving that city in both parents, and chooses the closer one to extend the 
        ///         tour. If one city has already appeared in the tour, we choose the other city. 
        ///         If both cities have already appeared, we randomly select a non-selected city."
        /// 
        /// </summary>
        /// <param name="parent1">A Parent Tour to be mated for crossover</param>
        /// <param name="parent2">A Parent Tour to be mated for crossover</param>
        /// <returns>The Route (ordered list of city traversal) created by combination of 2 parents (Child route)</returns>
        public static List<int> Crossover(Tour parent1, Tour parent2)
        {
            // Create a list called 'child' to hold the order of traversal to be inherited
            var child = new List<int>(Map.CityCount);

            // Create list of city numbers still available for inheritance
            var availableInheritance = new List<int>(Map.CityCount);
            for(var i = 0; i < Map.CityCount; i++)
                availableInheritance.Add(i);

            // Randomly select an index to start crossover
            var crossoverIndex = GeneticTsp.R.Next(availableInheritance.Count);

            // Start by adding the first city manually
            child.Add(parent1.Route[crossoverIndex]);
            var lastCityAdded = parent1.Route[crossoverIndex];

            // Remove that city from the list of available cities
            availableInheritance.RemoveAt(parent1.Route[crossoverIndex]);

            for (var i = 0; i < Map.CityCount - 1; i++)
            {
                // Inspect the city at the next index of parent1 (crossoverIndex could be written as indexWithinParent1)
                crossoverIndex = crossoverIndex == Map.CityCount - 1 ? 0 : crossoverIndex + 1;

                var cityNumberToInspect = parent1.Route[crossoverIndex];
                var indexWithinParent2 = parent2.Route.IndexOf(cityNumberToInspect);

                // Compare cities leaving that city in both parents
                int cityToInherit = -1;
                double closestDistance = -1;

                //
                // Check the FIRST parent's PREVIOUS city
                //
                var p1PrevIndex = crossoverIndex == 0 ? parent1.Route.Count - 1 : crossoverIndex - 1;
                var p1PrevCity = parent1.Route[p1PrevIndex];

                // Is it a city number that can still be inherited?
                if (availableInheritance.Contains(p1PrevCity))
                {
                    var thisDist = Map.DistanceBetweenCities(lastCityAdded, p1PrevCity);

                    // If a city hasn't been determined for inheritance yet, set as this
                    if (cityToInherit == -1 || thisDist < closestDistance)
                    {
                        cityToInherit = p1PrevCity;
                        closestDistance = thisDist;
                    }
                }

                //
                // Check the FIRST parent's NEXT city
                //
                var p1NextIndex = crossoverIndex == parent1.Route.Count - 1 ? 0 : crossoverIndex + 1;
                var p1NextCity = parent1.Route[p1NextIndex];

                // Is it a city number that can still be inherited?
                if (availableInheritance.Contains(p1NextCity))
                {
                    var thisDist = Map.DistanceBetweenCities(lastCityAdded, p1NextCity);

                    // If a city hasn't been determined for inheritance yet, set as this
                    if (cityToInherit == -1 || thisDist < closestDistance)
                    {
                        cityToInherit = p1NextCity;
                        closestDistance = thisDist;
                    }
                }

                //
                // Check the SECOND parent's PREVIOUS city
                //
                var p2PrevIndex = indexWithinParent2 == 0 ? parent2.Route.Count - 1 : indexWithinParent2 - 1;
                var p2PrevCity = parent2.Route[p2PrevIndex];

                // Is it a city that can still be inherited?
                if (availableInheritance.Contains(p2PrevCity))
                {
                    var thisDist = Map.DistanceBetweenCities(lastCityAdded, p2PrevCity);

                    // If a city hasn't been determined for inheritance yet, set as this
                    if (cityToInherit == -1 || thisDist < closestDistance)
                    {
                        cityToInherit = p2PrevCity;
                        closestDistance = thisDist;
                    }
                }

                //
                // Check the SECOND parent's NEXT city
                //
                var p2NextIndex = indexWithinParent2 == parent2.Route.Count - 1 ? 0 : indexWithinParent2 + 1;
                var p2NextCity = parent2.Route[p2NextIndex];

                // Is it a city that can still be inherited?
                if (availableInheritance.Contains(p2NextCity))
                {
                    var thisDist = Map.DistanceBetweenCities(lastCityAdded, p2NextCity);

                    // If a city hasn't been determined for inheritance yet, set as this
                    if (cityToInherit == -1 || thisDist < closestDistance)
                    {
                        cityToInherit = p2NextCity;
                        // ReSharper disable RedundantAssignment
                        closestDistance = thisDist;
                        // ReSharper restore RedundantAssignment
                    }
                }

                //
                // If no valid inheritance was found, choose a random available city to add
                //
                if (cityToInherit == -1)
                {
                    cityToInherit = availableInheritance.ElementAt(GeneticTsp.R.Next(availableInheritance.Count));
                }

                //
                // Add the inherited connection to the child, and remove it from available cities
                //
                child.Add(cityToInherit);
                lastCityAdded = cityToInherit;
                availableInheritance.Remove(cityToInherit);
            }

            return child;
        }

        public void Mutate()
        {
            var initialDistance = RouteDistance;

            var availableIndices = new List<int>(Route.Count);
            for (var i = 0; i < Route.Count; i++)
                availableIndices.Insert(i, i);

            // Choose the city positions to swap
            var city1Index = GeneticTsp.R.Next(availableIndices.Count);
            availableIndices.Remove(city1Index);
            var city2Index = GeneticTsp.R.Next(availableIndices.Count);

            // Do swap
            var tempCity = Route[city1Index];
            Route[city1Index] = Route[city2Index];
            Route[city2Index] = tempCity;

            var newDistance = CalculateDistance();

            // Revert the mutation if it doesn't improve the route distance
            if (newDistance > initialDistance)
            {
                // Swap the cities back
                tempCity = Route[city1Index];
                Route[city1Index] = Route[city2Index];
                Route[city2Index] = tempCity;

                // Reset distance
                RouteDistance = initialDistance;
            }
        }

        #endregion

        #region Interface and Override Methods

        public int CompareTo(Tour other)
        {
            // ReSharper disable RedundantIfElseBlock
            if (RouteDistance < other.RouteDistance)
                return -1;
            if (RouteDistance > other.RouteDistance)
                return 1;
            else
                return 0;
            // ReSharper restore RedundantIfElseBlock
        }

        public bool Equals(Tour other)
        {
            if (Route == null || other.Route == null) return false;

            if (Route.Count != other.Route.Count)
                return false;

            var thisIndex = Route.IndexOf(0);
            var otherIndex = other.Route.IndexOf(0);

            // ReSharper disable UnusedVariable
            foreach (var t in Route)
            {
                if (Route[thisIndex] != other.Route[otherIndex])
                    return false;

                if (thisIndex == Route.Count - 1)
                    thisIndex = 0;
                else
                    thisIndex++;

                if (otherIndex == other.Route.Count - 1)
                    otherIndex = 0;
                else
                    otherIndex++;
            }
            // ReSharper restore UnusedVariable

            return true;
        }

        public override string ToString()
        {
            var s = string.Format("Tour <{0:00000.0000}>: ", RouteDistance);
            s = Route.Aggregate(s, (current, i) => current + string.Format("{0:00} ", i));
            return s;
        }

        #endregion // Interface and Override Methods

        #endregion // Public Methods
    }
}
