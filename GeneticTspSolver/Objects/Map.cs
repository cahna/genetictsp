﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneticTspSolver.Objects
{
    public class Map
    {
        #region Attributes

        /// <summary>
        ///     Holds the number of cities in the most recently instantiated map.
        /// </summary>
        public static int CityCount { get; private set; }

        /// <summary>
        ///     Holds the cities of the most recently instantiated map.
        /// </summary>
        public static List<City> Cities;

        /// <summary>
        ///     Holds calculated distances between cities.
        ///     Accessible by reference to class in order to allow Tour objects to use the
        ///     distances lookup table without having their own copy of the array.
        /// </summary>
        public static double[,] DistancesBetweenCities { get; set; }

        public static double[,,] DistancesBetweenCitiesAndEdges { get; private set; }

        #endregion // Attributes

        #region Constructor

        public Map(List<City> cities)
        {
            //
            // Copy cities into map
            //
            Cities = TspData.CloneCityList(cities);

            //
            // (Re)Instantiate (public static) lookup table to hold distances between cities
            //
            DistancesBetweenCities = new double[Cities.Count, Cities.Count];

            //
            // (Re)Instantiate (public static) lookup table to hold lookup tables for each 
            // city's distances to all possible edges
            //
            DistancesBetweenCitiesAndEdges = new double[Cities.Count, Cities.Count, Cities.Count];
            Array.Clear(DistancesBetweenCitiesAndEdges, 0, (int)Math.Pow(Cities.Count, 3));

            //
            // Set city count
            //
            CityCount = Cities.Count;

            //
            // Calculate distances between all cities and fill lookup table
            //
            GenerateDistancesBetweenCities(Cities);
        }

        #endregion // Constructor

        #region Methods

        /// <summary>
        ///     Geometric distance equation.
        /// </summary>
        /// <param name="x1">X value of point 1 location</param>
        /// <param name="y1">Y Value of point 1 location</param>
        /// <param name="x2">X value of point 2 location</param>
        /// <param name="y2">Y value of point 2 location</param>
        /// <returns></returns>
        public static double Dist(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }

        /// <summary>
        ///     Looks up the distance previously generated in the lookup table
        /// </summary>
        /// <param name="city1"></param>
        /// <param name="city2"></param>
        /// <returns></returns>
        public static double DistanceBetweenCities(int city1, int city2)
        {
            int point1, point2;

            // Since only the upper half of the distances lookup table is filled,
            // the index must be looked up in the order of (Lesser ID, Greater ID)
            if (city1 > city2)
            {
                point1 = city2;
                point2 = city1;
            }
            else if (city1 < city2)
            {
                point1 = city1;
                point2 = city2;
            }
            else
            {
                // Could return 0 here, but for this program a result of 0 is an error
                throw new ArgumentException("node1 and node2 cannot be the same node");
            }

            return DistancesBetweenCities[point1, point2];
        }

        /// <summary>
        ///     Calculates the distance from a city to a line segment between
        ///     two other cities.
        /// </summary>
        /// <param name="testCity">City to test distance against</param>
        /// <param name="vertexA">City that comprises one vertex of line segment to test against</param>
        /// <param name="vertexB">City that comprises the other vertex of line segment to test against</param>
        /// <returns>
        ///     Distance from the city at testCityIndex to the line segment
        ///     connecting the cities at city1Index and city2Index
        /// </returns>
        public static double DistanceToLineSegment(City testCity, City vertexA, City vertexB)
        {
            var dist = ((vertexB - vertexA) ^ (testCity - vertexA)) / Math.Sqrt((vertexB - vertexA) * (vertexB - vertexA));

            var dot1 = (testCity - vertexB) * (vertexB - vertexA);
            if (dot1 > 0)
                return Math.Sqrt((vertexB - testCity) * (vertexB - testCity));

            var dot2 = (testCity - vertexA) * (vertexA - vertexB);

            return dot2 > 0 ? Math.Sqrt((vertexA - testCity) * (vertexA - testCity)) : Math.Abs(dist);
        }

        private static double RetrieveDistanceToLineSegment(int testCityIndex, int vertexAIndex, int vertexBIndex)
        {
            if (DistancesBetweenCitiesAndEdges == null)
                DistancesBetweenCitiesAndEdges = new double[Cities.Count,Cities.Count,Cities.Count];

            if(DistancesBetweenCitiesAndEdges[testCityIndex, vertexAIndex, vertexBIndex] == 0)
                DistancesBetweenCitiesAndEdges[testCityIndex, vertexAIndex, vertexBIndex] = DistanceToLineSegment(Cities[testCityIndex], Cities[vertexAIndex], Cities[vertexBIndex]);

            return DistancesBetweenCitiesAndEdges[testCityIndex, vertexAIndex, vertexBIndex];
        }

        /// <summary>
        /// 
        ///     Returns a lookup table (SortedDictionary) of the shortest distances to all available edges within
        ///     the current subroute.
        /// 
        ///         (double) Key   : Distance from city to edge starting at Value
        ///            (int) Value : Initial vertex of edge in left-to-right traversal of the route's array of cities
        /// 
        ///     Results of DistanceToLineSegment are cached (memoized) within public static DistancesBetweenCitiesAndEdges for
        ///     potential future calls.
        /// 
        /// </summary>
        /// <param name="currentLinkedSubRoute"></param>
        /// <param name="cityToAdd"></param>
        /// <returns></returns>
        public static SortedDictionary<double, int> GetDistanceFromAllAvailableEdges(LinkedList<int> currentLinkedSubRoute, int cityToAdd)
        {
            //
            // Create List from LinkedList
            //
            var currentSubRoute = new List<int>(currentLinkedSubRoute);

            //
            // Create a lookup table to cache the distances from the cityToAdd to all
            // available line segments. Key is distance (double). Value is initial
            // node of vertex.
            //
            var distancesFromEdges = new SortedDictionary<double, int>();

            //
            // Calculate distances and fill lookup table
            //
            for (var i = 0; i < currentSubRoute.Count; i++)
            {
                // Choose vertices as 2 connected cities from within route (being the next city in the route indicates connection)
                var vertex1City = currentSubRoute[i];
                var vertex2City = i == currentSubRoute.Count - 1 ? currentSubRoute[0] : currentSubRoute[i + 1];

                //
                // Retrieve (from cache or calculate) shortest distance between the city to be added and current connection line segment
                //
                var dist = RetrieveDistanceToLineSegment(cityToAdd, vertex1City, vertex2City);

                // Add the distance to the edge if it is unique
                if (!distancesFromEdges.ContainsKey(dist))
                    distancesFromEdges.Add(dist, vertex1City);
                else
                {
                    //
                    // CONFLICT HEURISTIC: If the distance calculated previously exists as a key within the distances lookup table, 
                    // then the city to add is equidistant from two line segments currently connected within the route. This occurs
                    // when the closest distance between a city and two line segments is a city (vertex), itself, and - as such - there
                    // exists no line between the city and the edge connecting the two vertices that is perpendicular to the line segment
                    // connecting the two vertices. Thus, the city to add is equidistant from 2 edges (line segments) already connected 
                    // within the route. Associate the shorter line segment with the distance key within the lookup table by associating
                    // the initial vertex city for the shorter segment with the existing distance key.
                    //
                    // Grab the city that already exists as the initial city for the line segment.
                    //
                    //     NOTE: Since city order within the array genome for this GA determiines the route traversal order,
                    //           it can be inferred that if an existing distance key is found within the lookup table, then the
                    //           existing value associated with that key is -- in fact -- the city immediately before vertex1
                    //           within the route. Example crappy visualization:
                    //
                    //                 v2               v0
                    //                   \          __-` 
                    //                    \    __--`
                    //                     v1 `
                    //                     |
                    //                     |
                    //                   city
                    //
                    var vertex0City = distancesFromEdges[dist];

                    //
                    // Calculate the distances between vertex0 and vertex2 from the city to be added
                    //
                    var vertex0Dist = DistanceBetweenCities(cityToAdd, vertex0City);
                    var vertex2Dist = DistanceBetweenCities(cityToAdd, vertex2City);

                    //
                    // If the city is closest to vertex2, associate the distance key with vertex1.
                    // Otherwise, leave the existing value in place and don't modify.
                    //
                    if (vertex2Dist < vertex0Dist)
                        distancesFromEdges[dist] = vertex1City;
                }
            }

            return distancesFromEdges;
        }

        /// <summary>
        ///     Calculates distances between all cities and stores them in the upper half of the distances lookup table.
        /// </summary>
        public static void GenerateDistancesBetweenCities(IList<City> cities)
        {
            for (var row = 0; row < cities.Count; row++)
            {
                for (var col = row + 1; col < cities.Count; col++)
                {
                    var x1 = cities[row].X;
                    var y1 = cities[row].Y;
                    var x2 = cities[col].X;
                    var y2 = cities[col].Y;
                    DistancesBetweenCities[row, col] = Dist(x1, y1, x2, y2);
                }
            }
        }

        #endregion
    }
}
