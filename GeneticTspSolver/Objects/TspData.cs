﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace GeneticTspSolver.Objects
{
    public class TspData
    {
        #region Class Variables

        public string Name { get; private set; }
        public string Type { get; private set; }
        public string EdgeWeightType { get; private set; }
        public int Dimension { get; private set; }
        public List<City> Nodes { get; private set; }
        public bool[,] Connections { get; private set; }
        public bool IsTree { get; private set; }
        public bool ParseSuccess { get; private set; }

        #endregion // Class Variables

        #region Public Static
        /// <summary>
        ///     Creates a new unique list of nodes by exactly copying the contents of the input array of
        ///     nodes to a new list of nodes
        /// </summary>
        /// <param name="nodes">Array of nodes to be cloned into a list</param>
        /// <returns>List of nodes unique to this tour</returns>
        public static List<City> CloneCityList(IReadOnlyCollection<City> nodes)
        {
            var tourNodes = new List<City>(nodes.Count);
            tourNodes.AddRange(nodes.Select(n => new City(n)));
            return tourNodes;
        }

        #endregion // Public Static

        #region Constructor

        /// <summary>
        ///     Reads and parses the input file. Sets class variables to parsed data.
        /// </summary>
        public TspData(string inFile)
        {
            ParseSuccess = false;
            string text;

            // Try to open file
            try
            {
                text = File.ReadAllText(inFile);
            }
            catch (IOException)
            {
                throw new IOException("Invalid input file!");
            }

            bool headerSuccess = ParseHeader(text);
            IsTree = AreConnections(text);
            bool coordinatesSuccess = ParseCoordinates(text);

            ParseSuccess = headerSuccess && coordinatesSuccess;

            if (IsTree && ParseSuccess)
            {
                IsTree = true;
                Connections = new bool[Dimension,Dimension];
                ParseSuccess = ParseSuccess && ParseConnections(text);
            }
        }

        #endregion // Constructor

        #region Private Methods

        private bool ParseHeader(string inText)
        {
            const string regex = @"^NAME: (?<name>\w+).*TYPE: (?<type>\w+).*DIMENSION: (?<dim>\d+).*EDGE_WEIGHT_TYPE: (?<ewt>\w+).*NODE_COORD_SECTION";
            var m = Regex.Match(inText, regex, RegexOptions.Singleline);

            if (!m.Success)
                return false;

            Name = m.Groups["name"].Value;
            Type = m.Groups["type"].Value;
            try
            {
                Dimension = int.Parse(m.Groups["dim"].Value);
            }
            catch (Exception)
            {
                Console.WriteLine(@"Unable to parse value from regex capture!");
                return false;
            }
            EdgeWeightType = m.Groups["ewt"].Value;
            return true;
        }

        private bool ParseCoordinates(string inText)
        {
            // First capture the block from the string that contains the list of coordinates
            const string captRegex = @".*NODE_COORD_SECTION\r\n(?<coord_section>.*)[NODE_CONNECT_SECTION]?.*\z";
            var m = Regex.Match(inText, captRegex, RegexOptions.Singleline);

            if (!m.Success)
            {
                Console.WriteLine(@"Could not find coordinates list!");
                return false;
            }

            // coord_section holds the text blob where the list of coordinates is to be parsed
            string coordSection = m.Groups["coord_section"].Value;

            // First use regex to capture each line of coordinate data
            var lineRegex = new Regex(@"(?>\d+ \d+.\d+ \d+.\d+)\r\n", RegexOptions.Multiline);

            // Then use regex to capture the n, x, and y values from the line
            var coordRegex = new Regex(@"(?<n>\d+) (?<x>\d+.\d+) (?<y>\d+.\d+)", RegexOptions.Singleline);

            // Create a new coordinate for each and add it to the queue to be returned
            var tempCoordinates = new Queue<City>();
            var coordinatesFound = 0;

            // Capture all of the line capture matches
            var lines = lineRegex.Matches(coordSection);

            // Line regex capture loop
            foreach (var value in from object line in lines select coordRegex.Matches(line.ToString()) into values from Match value in values select value)
            {
                int n;
                double x;
                double y;

                try
                {
                    n = int.Parse(value.Groups["n"].Value);
                    x = double.Parse(value.Groups["x"].Value);
                    y = double.Parse(value.Groups["y"].Value);
                }
                catch (Exception)
                {
                    Console.WriteLine(@"Unable to parse value from regex capture!");
                    return false;
                }

                // Add new Coordinate to queue
                tempCoordinates.Enqueue(new City(n, x, y));
                coordinatesFound++;
            }

            // Perform some sanity checks once the parsing is finished to roughly check for mistakes
            if (coordinatesFound == 0 || tempCoordinates.Count == 0)
            {
                Console.WriteLine(@"Did not parse any coordinates from file!");
                return false;
            }

            if (coordinatesFound != tempCoordinates.Count || coordinatesFound != Dimension)
            {
                Console.WriteLine(@"Coordinates were parsed, but an error has occured in parsing. The number of coordinates found do not line up. Please check input file sanity.");
                return false;
            }

            Nodes = tempCoordinates.ToList();
            return true;
        }

        private static bool AreConnections(string inText)
        {
            const string captRegex = @".*NODE_CONNECT_SECTION\r\n[\d+ \d+\r\n]+.*";
            var m = Regex.Match(inText, captRegex, RegexOptions.Singleline);
            return m.Success;
        }

        private bool ParseConnections(string inText)
        {
            // First capture the block from the string that contains the list of coordinates
            const string captRegex = @".*NODE_CONNECT_SECTION\r\n(?<conn_section>[\d+ \d+\r\n]+\d+ \d+$)";
            var m = Regex.Match(inText, captRegex, RegexOptions.Singleline);

            if (!m.Success)
            {
                Console.WriteLine(@"Could not find connections list!");
                return false;
            }

            string connSection = m.Groups["conn_section"].Value;

            // First use regex to capture each line of connection data
            var lineRegex = new Regex(@"(?>\d+ \d+)(?:\r\n|.*)", RegexOptions.Multiline);

            // Then use regex to capture the c1 and c2 values from the line
            var connRegex = new Regex(@"(?<c1>\d+) (?<c2>\d+)", RegexOptions.Singleline);

            // Capture all of the line capture matches
            var lines = lineRegex.Matches(connSection);

            // Line regex capture loop
            foreach (Match connection in lines.Cast<Match>().Select(line => connRegex.Matches(line.ToString())).SelectMany(conns => conns.Cast<Match>()))
            {
                int c1;
                int c2;
                try
                {
                    c1 = int.Parse(connection.Groups["c1"].Value);
                    c2 = int.Parse(connection.Groups["c2"].Value);
                }
                catch (Exception)
                {
                    Console.WriteLine(@"Unable to parse integer from regex capture!");
                    return false;
                }
                // Set connection in truth table appropriately
                Connections[c1 - 1, c2 - 1] = true;
            }

            return true;
        }

        #endregion // Private Methods
    }
}