﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace GeneticTspSolver.Objects
{
    public class GeneticTsp
    {
        #region Class Variables

        /// <summary>
        ///     Stopwatch to keep track of runtime
        /// </summary>
        protected readonly Stopwatch RunTimeWatch;

        /// <summary>
        ///     Every [(City.Count / GroupSize) * Population.Count] generations,
        ///     check to see if a migration action needs to be performed.
        /// </summary>
        protected readonly int MigrationInterval;

        protected int LastMigration;

        protected readonly int WindowSize;

        protected Queue<double> PopulationAverageWindow;

        protected Queue<double> PopulationRangeWindow;

        /// <summary>
        ///     Data parsed from .tsp input file
        /// </summary>
        public TspData GTspData { get; private set; }

        /// <summary>
        ///     Map of cities from .tsp file including a lookup table
        ///     of all connection distances from each city to every other
        ///     city
        /// </summary>
        public Map GMap { get; private set; }

        /// <summary>
        ///     List of tours that form the GA population
        /// </summary>
        public List<Tour> Population { get; private set; }

        /// <summary>
        ///     Keeps track of how many generations have been iterated through
        /// </summary>
        public int Generations { get; private set; }

        #endregion // Class Variables

        #region Static Attributes

        /// <summary>
        ///     Single random number generator project
        /// </summary>
        public static Random R = new Random();

        #endregion // Static Attributes

        #region Constructor
        
        /// <summary>
        /// 
        ///     Instantiates a new Genetic Algorithm control object to use
        ///     survival of the fittest and natural selection techniques to 
        ///     find shortest Hamiltonian Cycle paths for traversing a list of
        ///     cities. Also performs the following important tasks:
        ///         
        ///         - Opens and parses the specified input file for TSP data
        ///         - Instantiates a new Map based off of the parsed data
        ///         - Instantiates (and seeds, if requested) a global random for
        ///           all other classes to use
        ///         - Generates a new population ot Tours based off of the 
        ///           settings set within the public static AlgorithmSettings class.
        ///         - Sorts the population based off of tour route distance in
        ///           ascending order
        ///         
        /// </summary>
        /// <param name="infile">.tsp file containing the city information to use</param>
        /// <param name="seed">Optional seed for universal random number generator</param>
        public GeneticTsp(string infile, int seed = -1)
        {
            // Parse the TSP data file
            GTspData = new TspData(infile);

            // Create a new map based upon the data
            GMap = new Map(GTspData.Nodes);

            // Create Universal Random
            R = seed != -1 ? new Random(seed) : new Random();

            // Create the stopwatch for benchmarking
            RunTimeWatch = new Stopwatch();

            // Must include initial connection in timing
            RunTimeWatch.Start();

            // Create population
            Population = new List<Tour>(AlgorithmSettings.PopulationSize);
            for(var i = 0; i < AlgorithmSettings.PopulationSize; i++)
                Population.Add(new Tour(Map.CityCount, AlgorithmSettings.InitialConnectionMethod));

            // Set migration interval
            MigrationInterval = (GTspData.Nodes.Count/AlgorithmSettings.GroupSize)*AlgorithmSettings.PopulationSize;
            LastMigration = 0;

            // Create windows at specified sizes
            WindowSize = (int)Math.Floor(MigrationInterval * AlgorithmSettings.MigrationWindowPercent);
            PopulationAverageWindow = new Queue<double>(WindowSize);
            PopulationRangeWindow = new Queue<double>(WindowSize);

            // Set state to initial generation
            Generations = 0;

            // Sort the population
            Population.Sort();

            // Stop the watch for now
            RunTimeWatch.Stop();
        }

        public GeneticTsp(TspData data, List<Tour> pop = null)
        {
            // Parse the TSP data file
            GTspData = data;

            // Create a new map based upon the data
            GMap = new Map(GTspData.Nodes);

            // Create the stopwatch for benchmarking
            RunTimeWatch = new Stopwatch();

            // Create Universal Random
            R = new Random();

            // Must include initial connection in timing
            RunTimeWatch.Start();

            // Create population
            if (pop == null)
            {
                Population = new List<Tour>(AlgorithmSettings.PopulationSize);
                for (var i = 0; i < AlgorithmSettings.PopulationSize; i++)
                    Population.Add(new Tour(Map.CityCount, AlgorithmSettings.InitialConnectionMethod));
            }
            else
            {
                Population = pop;
            }

            // Set migration interval
            MigrationInterval = (GTspData.Nodes.Count / AlgorithmSettings.GroupSize) * AlgorithmSettings.PopulationSize;
            LastMigration = 0;

            // Create windows at specified sizes
            WindowSize = (int)Math.Floor(MigrationInterval * AlgorithmSettings.MigrationWindowPercent);
            PopulationAverageWindow = new Queue<double>(WindowSize);
            PopulationRangeWindow = new Queue<double>(WindowSize);

            // Set state to initial generation
            Generations = 0;

            // Sort the population
            Population.Sort();

            // Stop the watch for now
            RunTimeWatch.Stop();
        }

        #endregion // Constructor

        #region Private Methods

        private bool Migration()
        {
            if (!AlgorithmSettings.UseMigration)
                return false;

            //
            // Calculate population statistics
            //
            var distanceRange = Population.Max().RouteDistance - Population.Min().RouteDistance;
            double distanceTotal = 0;
            distanceTotal += Population.Sum(t => t.RouteDistance);
            var distanceAverage = distanceTotal / Population.Count;

            //
            // Add statistics for this generation to the window
            //
            if (PopulationRangeWindow.Count >= WindowSize)
                PopulationRangeWindow.Dequeue();
            PopulationRangeWindow.Enqueue(distanceRange);

            if (PopulationAverageWindow.Count >= WindowSize)
                PopulationAverageWindow.Dequeue();
            PopulationAverageWindow.Enqueue(distanceAverage);

            //
            // Is it time to check for a migration?
            //
            if (Generations % MigrationInterval == 0 || ((Generations - LastMigration > MigrationInterval) && Generations % Population.Count == 0))
            {
                //
                // Check to see if the slopes of the PopulationRangeWindow and PopulationAverageWindow data have levelled off
                //
                var mA = (PopulationAverageWindow.Last() - PopulationAverageWindow.First()) / PopulationAverageWindow.Count;
                var mR = (PopulationRangeWindow.Last() - PopulationRangeWindow.First()) / PopulationRangeWindow.Count;

                //
                // Note to self: If the slopes are increasing, should I add more entropy (genetic data) to the population, or break?
                //

                //
                // If the slopes have levelled off, select the bottom (1.0 - MigrationWindowPercent)% of Population for Migration
                //
                if (Math.Abs(mA) < AlgorithmSettings.MinimumSlopeThreshold && Math.Abs(mR) < AlgorithmSettings.MinimumSlopeThreshold)
                {
                    //
                    // Remove all tours in the top MigrationWindowPercent of the population that are duplicates leaving only 
                    // unique versions of the most-fit tours
                    //
                    var indexToStartReplacements = (int)Math.Ceiling(Population.Count * AlgorithmSettings.MigrationWindowPercent);
                    var uniqueTours = new List<Tour>();
                    var indicesToReplace = new List<int>();

                    //
                    // Find which tours in the top MigrationWindowPercent of the population are duplicates, and need to be replaced
                    //
                    for (var i = (int)Math.Ceiling(indexToStartReplacements * AlgorithmSettings.MigrationWindowPercent); i < indexToStartReplacements; i++)
                    {
                        if(uniqueTours.Contains(Population[i]))
                            indicesToReplace.Add(i);
                        else
                            uniqueTours.Add(Population[i]);
                    }

                    //
                    // Replace those duplicate tours
                    //
                    foreach(var i in indicesToReplace)
                        Population[i] = new Tour(GTspData.Nodes.Count, AlgorithmSettings.InitialConnectionMethod);

                    //
                    // Replace the least fit tours with new tours to increase genetic data
                    //
                    for (var i = indexToStartReplacements; i < Population.Count; i++)
                    {
                        Population[i] = new Tour(GTspData.Nodes.Count, AlgorithmSettings.InitialConnectionMethod);
                    }

                    //
                    // Set last migration to current generation and resort the population
                    //
                    LastMigration = Generations;
                    return true;
                }
            }

            return false;
        }

        #endregion // Private Methods

        #region Public Methods
        
        /// <summary>
        ///     Performs crossover and mutation upon the population.
        /// </summary>
        /// <returns>True if a new shortest tour was generated. False if not.</returns>
        public bool NextGeneration()
        {
            //
            // Save the current shortest distance for comparison after crossover
            //
            var shortestTourDistance = Population.First().RouteDistance;

            //
            // Start the stopwatch for benchmarking
            //
            RunTimeWatch.Start();

            //
            // Randomly select tours from the population to create the inheritance group
            //
            var selectableTourIndices = new List<int>(Population.Count);
            for(var i = 0; i < Population.Count; i++)
                selectableTourIndices.Insert(i,i);

            var group = new List<Tour>(AlgorithmSettings.GroupSize);
            var groupTourIndexLookup = new Dictionary<Tour, int>();

            for (var i = 0; i < AlgorithmSettings.GroupSize; i++)
            {
                var randomIndex = R.Next(selectableTourIndices.Count);
                var populationIndex = selectableTourIndices[randomIndex];
                group.Add(Population[populationIndex]);
                groupTourIndexLookup.Add(Population[populationIndex], populationIndex);
                selectableTourIndices.Remove(populationIndex);
            }

            //
            // Sort the group by tour route distance
            //
            group = group.OrderBy(g => g.RouteDistance).ToList();

            //
            // Mate the two fittest tours within the group to create 2 child routes
            //
            var route1 = Tour.Crossover(group[0], group[1]);
            var route2 = Tour.Crossover(group[1], group[0]);

            //
            // Create the child tours from the resultant routes from crossover
            //
            var child1 = new Tour(route1);
            var child2 = new Tour(route2);

            //
            // Perform mutation if its time for it
            //
            if(R.NextDouble() < AlgorithmSettings.MutationRate)
                child1.Mutate();
            if(R.NextDouble() < AlgorithmSettings.MutationRate)
                child2.Mutate();

            //
            // If either of the children are more fit than the least fit of the group
            // replace those tours within the population with the more fit child
            //
            var child1Dist = child1.RouteDistance;
            var child2Dist = child2.RouteDistance;
            var gLastDist = group[group.Count - 1].RouteDistance;
            var g2LastDist = group[group.Count - 2].RouteDistance;

            var gLastReplaced = false;
            var g2LastReplaced = false;

            if (child1Dist < g2LastDist)
            {
                Population[groupTourIndexLookup[group[group.Count - 2]]] = child1;
                g2LastReplaced = true;
            }
            else if (child1Dist < gLastDist)
            {
                Population[groupTourIndexLookup[group[group.Count - 1]]] = child1;
                gLastReplaced = true;
            }

            if (child2Dist < g2LastDist && !g2LastReplaced)
            {
                Population[groupTourIndexLookup[group[group.Count - 2]]] = child2;
                g2LastReplaced = true;
            }
            else if (child2Dist < gLastDist && !gLastReplaced)
            {
                Population[groupTourIndexLookup[group[group.Count - 1]]] = child2;
                gLastReplaced = true;
            }

            //
            // Perform immigration if necessary
            //
            var migrationPerformed = Migration();

            //
            // Stop stopwatch
            //
            RunTimeWatch.Stop();

            //
            // Increment Generations Counter
            //
            Generations++;

            //
            // Resort the population if it was altered and return whether a new shortest
            // tour was found
            //
            if (gLastReplaced || g2LastReplaced || migrationPerformed)
            {
                Population.Sort();
                if (Population.First().RouteDistance < shortestTourDistance)
                    return true;
            }

            //
            // Finally, it can be reasoned that a shortest tour was not generated in this generation
            //
            return false;
        }

        private static string csvLine(IEnumerable<object> data)
        {
            var s = new StringBuilder();
            foreach (var o in data)
                s.Append(o + ",");
            return s.ToString();
        }

        public void RunFor(int generations, string prefix = "")
        {
            var writestats = !string.IsNullOrEmpty(prefix);
            var headers = new List<object> {"Generations", "Shortest Tour", "Range", "Average"};

            if(writestats)
            {
                using (var f = new StreamWriter(prefix + AlgorithmSettings.GenerateRunOutputFilename() + ".csv"))
                {
                    f.WriteLine(csvLine(headers));
                    for (var i = 0; i < generations; i++)
                    {
                        NextGeneration();

                        //
                        // Calculate population statistics
                        //
                        var distanceRange = Population.Max().RouteDistance - Population.Min().RouteDistance;

                        double distanceTotal = 0;
                        distanceTotal += Population.Sum(t => t.RouteDistance);

                        var distanceAverage = distanceTotal/Population.Count;

                        lock (f)
                        {
                            f.WriteLineAsync(csvLine(new List<object>
                                {
                                    Generations,
                                    Population.First().RouteDistance,
                                    distanceRange,
                                    distanceAverage
                                }));
                        }
                    }
                }
            }
            else
            {
                for (var i = 0; i < generations; i++)
                    NextGeneration();
            }
        }

        #endregion // Public Methods
    }
}
