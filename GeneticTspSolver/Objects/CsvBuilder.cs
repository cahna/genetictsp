﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace GeneticTspSolver.Objects
{
    public class CsvBuilder
    {
        private const string FilePrefix = @"../../../GeneticTspSolver/Data/Outputs/";
        private readonly string _csvFileName;
        private readonly StreamWriter _file;
        private ConcurrentQueue<List<object>> _writeQueue;
        private readonly BackgroundWorker _worker;

        public CsvBuilder(string filename, List<object> columnHeaders)
        {
            // Open file
            _csvFileName = FilePrefix + filename + ".csv";
            _file = new StreamWriter(_csvFileName, File.Exists(_csvFileName));
            
            // Set up shared resourses and mutex
            _writeQueue = new ConcurrentQueue<List<object>>();

            // Write column headers
            AddRow(columnHeaders);

            // Setup bg worker
            _worker = new BackgroundWorker {WorkerReportsProgress = false, WorkerSupportsCancellation = true};
            _worker.DoWork += _cvsWorker_DoWork;
            _worker.RunWorkerCompleted += _cvsWorker_RunWorkerCompleted;

            _worker.RunWorkerAsync();
        }

        private void _cvsWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;

            if (worker == null)
                throw new Exception("Worker is null");

            while (!worker.CancellationPending)
            {
                if (_writeQueue.Count > 100)
                {
                    List<object> dequeued;
                    while (_writeQueue.TryDequeue(out dequeued))
                    {
                        WriteRow(dequeued);
                    }
                }
            }

            if (worker.CancellationPending)
            {
                List<object> dequeued;
                while (_writeQueue.Count > 0 && _writeQueue.TryDequeue(out dequeued))
                {
                    WriteRow(dequeued);
                }
            }
        }

        private void _cvsWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _file.Close();
            Console.WriteLine("CSV BG WORKER COMPLETED");
        }

        private void WriteRow(List<object> cellData)
        {
            for (var i = 0; i < cellData.Count; i++)
            {
                var suffix = ",";

                if (i == cellData.Count - 1)
                    suffix = "\n";

                _file.Write(cellData[i] + suffix);
            }
        }

        public void CancelWorker()
        {
            _worker.CancelAsync();
        }

        public void AddRow(List<object> cellData)
        {
            if (_writeQueue == null) 
                _writeQueue = new ConcurrentQueue<List<object>>();
                
            _writeQueue.Enqueue(cellData);
        }
    }
}
