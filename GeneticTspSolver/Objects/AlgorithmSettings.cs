﻿using System;

namespace GeneticTspSolver.Objects
{
    public static class AlgorithmSettings
    {
        public static Tour.Connection InitialConnectionMethod = Tour.Connection.GreedyPointToLine;
        public static double UseClosestCityRate = 0.85;
        public static double MutationRate = 0.35;
        public static int PopulationSize = 1000;
        public static int GroupSize = 5;
        public static double MigrationWindowPercent = 0.25;
        public static double MinimumSlopeThreshold = 0.005;
        public static bool UseMigration = true;

        public static string GenerateRunOutputFilename()
        {
            var connMethod = Enum.GetName(typeof (Tour.Connection), InitialConnectionMethod);

            if (InitialConnectionMethod == Tour.Connection.ClosestWithRandom)
                connMethod += string.Format("_R[{0}]", UseClosestCityRate);

            return string.Format("{0}_{1}_P[{2}]_M[{3}]_G[{4}]", DateTime.Now.ToString("dd.MM_HH.mm.ss"), connMethod, PopulationSize, MutationRate, GroupSize);
        }
    }
}
