﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using GeneticTspSolver.Objects;
using System.Diagnostics;

namespace GeneticTspSolver
{
    class Program
    {
        private const string InFileTemp = @"../../Data/Random{0}.tsp";
        private const string Infile30 = @"../../Data/Random30.tsp";
        private const int crowdCount = 10;
        private const int crowdGenerations = 10000;
        private const int gaGenerations = 10000;

        public static double WOC(string infile, bool useWocPop)
        {
            // Parse the TSP data file
            var tspData = new TspData(string.Format(InFileTemp, 11));

            // Generate the crowds
            Console.WriteLine("Generating the crowds and running them...");
            var s = new Stopwatch();
            s.Start();
            var crowd = new GeneticTsp[crowdCount];
            var runActions = new Task[crowdCount];
            for (var i = 0; i < crowdCount; i++)
            {
                crowd[i] = new GeneticTsp(tspData);
                var i1 = i;
                runActions[i] = Task.Factory.StartNew(() => crowd[i1].RunFor(crowdGenerations));
            }
            Task.WaitAll(runActions);

            // Grab the top 10% of tours in each population
            //Console.WriteLine("Determining experts");
            var topTours = new List<Tour>();
            foreach (var g in from g in crowd let uniqueTours = g.Population.Distinct().OrderBy(t => t.RouteDistance) select g)
            {
                for (var i = 0; i < AlgorithmSettings.PopulationSize * 0.10; i++)
                    topTours.Add(g.Population[i]);
            }

            // Generate "Wisdom of crowds". Start by summing connection counts for each tour in selected experts from crowd
            //Console.WriteLine("Aggregating crowd data");
            var width = tspData.Dimension;
            var connectionSums = new double[width, width];
            foreach (var tour in topTours)
            {
                for (var i = 0; i < tour.Route.Count - 1; i++)
                {
                    var a = tour.Route[i];
                    var b = tour.Route[i + 1];
                    var n1 = a < b ? a : b;
                    var n2 = a < b ? b : a;
                    connectionSums[n1, n2]--;
                }
            }

            // Make a new GA
            //Console.WriteLine("Running GA based upon aggregated connection weights");
            var pop = useWocPop ? topTours : null;
            var wocGa = new GeneticTsp(tspData, pop);

            // Replace the route distances table with the connectionSums so the new GA tries to select
            // connections that are more "popular" from the crowd
            Map.DistancesBetweenCities = connectionSums;

            // Run the GA
            wocGa.RunFor(gaGenerations, @"../../Data/Outputs/WOC-");

            // Calculate the actual route distances table
            Map.GenerateDistancesBetweenCities(tspData.Nodes);

            foreach (var t in wocGa.Population)
                t.UpdateDistance();

            s.Stop();
            Console.WriteLine("WoC Finished" + (s.ElapsedMilliseconds/1000.000));

            return wocGa.Population.OrderBy(t => t.RouteDistance).First().RouteDistance;
        }

        public static double NormalGA(string infile)
        {
            var s = new Stopwatch();
            Console.WriteLine("Running normal GA");
            s.Start();
            var ga = new GeneticTsp(string.Format(InFileTemp, 11));
            ga.RunFor(gaGenerations, @"../../Data/NORMALGA-");
            s.Stop();
            Console.WriteLine("Normal GA finished: " + (s.ElapsedMilliseconds/1000.000));
            return ga.Population.OrderBy(t => t.RouteDistance).First().RouteDistance;
        }

        // ReSharper disable FunctionNeverReturns
        static void Main()
        {
            for (var i = 0; i < 3; i++)
            {
                var regGa = NormalGA(Infile30);
                var wocBest = WOC(Infile30, false);
                var woc2Best = WOC(Infile30, true);

                Console.WriteLine();
                Console.WriteLine("  GA Best: {0}", regGa);
                Console.WriteLine("WoC1 Best: {0}", wocBest);
                Console.WriteLine("WoC2 Best: {0}", woc2Best);
            }
            Console.ReadLine();
        }
        // ReSharper restore FunctionNeverReturns

        private static void _geneticWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var a = e.UserState as DrawTourArgs;
            if (a != null) Console.WriteLine("{0} : {1}", a.StopWatchMillis, a.TourToDraw);
        }

        private static void _geneticWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine("WORKER DONE!");
        }
    }
}
