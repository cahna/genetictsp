﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using GeneticTspSolver.Objects;
using Microsoft.Win32;

namespace GeneticTspSolverGui
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region Private Variables

        private string _tspFileName;
        private ParallelGeneticTsp _genetic;
        private BackgroundWorker _geneticWorker;
        private int _progressMax = 1000;

        #endregion // Private Variables

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion // Constructor

        #region Genetic Methods

        private void _createNewGeneticSolver()
        {
            if (_tspFileName == null)
            {
                MessageBox.Show("Open a TSP file first!");
                return;
            }

            // Check settings
            try
            {
                // ReSharper disable ReturnValueOfPureMethodIsNotUsed
                int.Parse(PopulationSizeTextBox.Text);
                // ReSharper restore ReturnValueOfPureMethodIsNotUsed
            }
            catch (Exception)
            {
                MessageBox.Show("Population Size is invalid!");
                return;
            }

            _applyAlgorithmSettings();

            _genetic = null;
            _genetic = new ParallelGeneticTsp(_tspFileName);
        }

        private void _applyAlgorithmSettings()
        {
            AlgorithmSettings.PopulationSize = int.Parse(PopulationSizeTextBox.Text);
            AlgorithmSettings.MutationRate = MutationRateSlider.Value;
            AlgorithmSettings.GroupSize = (int)GroupSizeSlider.Value;
            AlgorithmSettings.UseClosestCityRate = UseClosestSlider.Value;
            AlgorithmSettings.InitialConnectionMethod =
                (Tour.Connection) Enum.Parse(typeof (Tour.Connection), InitialConnectDropDown.SelectedItem.ToString());
        }
        
        private void _resetGeneticWorker()
        {
            if(_geneticWorker != null)
                _geneticWorker.Dispose();

            _geneticWorker = new BackgroundWorker {WorkerReportsProgress = true, WorkerSupportsCancellation = true};
            _geneticWorker.DoWork += _genetic.Genetic_DoWork;
            _geneticWorker.RunWorkerCompleted += _geneticWorkerStep_RunWorkerCompleted;
            _geneticWorker.ProgressChanged += _geneticWorker_ProgressChanged;
        }

        private void _geneticWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _updateProgressBar(e.ProgressPercentage);

            if (e.UserState == null) return;

            var a = e.UserState as DrawTourArgs;
            if (a == null) return;

            RunTimeLabel.Content = a.StopWatchMillis / 1000.000 + " s";
            ShortestTourLabel.Content = a.TourToDraw.RouteDistance;
            GenerationLabel.Content = e.ProgressPercentage;
            DrawConnections(a.TourToDraw.Route);
            _updatePopulationListBoxContents(a.PopulationDescriptions);
        }

        private void _updatePopulationListBoxContents(IEnumerable<string> populationDescriptions)
        {
            TourListBox.Items.Clear();
            if (_genetic == null) return;

            foreach (var t in populationDescriptions)
                TourListBox.Items.Add(t);
        }

        private void _updateProgressBar(int p)
        {
            if (p >= _progressMax)
            {
                _progressMax += _progressMax;
                ControlProgressBar.Maximum = _progressMax;
            }
            ControlProgressBar.Value = p;
        }

        private void _geneticWorkerStep_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string message;
            if (e.Cancelled)
            {
               message = @"Background worker cancelled. Back to control thread.";
            }
            else if (e.Error != null)
            {
                message = "Error: " + e.Error.Message;
            }
            else
            {
                message = "Worker stopped";
                // Enable buttons (except the stop button)
                RunButton.IsEnabled = true;
                ResetButton.IsEnabled = true;
                StopButton.IsEnabled = false;
            }

            MessageBox.Show(message);
        }

        #endregion // Genetic Methods

        #region Graphics Methods

        private void _clearMap()
        {
            // Delete existing cities that have been drawn
            CityCanvas.Children.RemoveRange(1, CityCanvas.Children.Count);

            // Delete links that have been drawn
            LinkCanvas.Children.RemoveRange(0, LinkCanvas.Children.Count);
        }

        private void DrawCities(List<City> cities)
        {
            _clearMap();

            var redBrush = new SolidColorBrush(Colors.Red);
            var maxX = (from city in cities select city.X).Max();
            var canvasX = CityCanvas.ActualHeight - 20;
            var ratio = canvasX / maxX;

            foreach (var cityCircleGraphic in cities.Select(node => new Ellipse
                {
                    Fill = redBrush,
                    StrokeThickness = 1,
                    Stroke = Brushes.Black,
                    Width = 5,
                    Height = 5,
                    Margin = new Thickness(node.X * ratio, node.Y * ratio, 0, 0)
                }))
            {
                CityCanvas.Children.Add(cityCircleGraphic);
            }
        }

        private void DrawConnections(List<int> connectionOrder)
        {
            if(_genetic == null)
                throw new Exception("Genetic solver object is null...");

            // Remove any connections that have been previously drawn
            _clearConnections();

            var canvasX = LinkCanvas.ActualHeight - 20;
            double maxX = -1;
            foreach (var cityNumber in connectionOrder)
            {
                const double epsilon = 0.001;
                if (Math.Abs(maxX - -1) < epsilon || _genetic.GTspData.Nodes[cityNumber].X > maxX)
                {
                    maxX = _genetic.GTspData.Nodes[cityNumber].X;
                }
            }
            var ratio = canvasX / maxX;

            for (var i = 0; i < connectionOrder.Count; i++)
            {
                var nextIndex = i == connectionOrder.Count - 1 ? 0 : i + 1;
                var line = new Line
                    {
                        Stroke = Brushes.Black,
                        StrokeThickness = 1,
                        X1 = _genetic.GTspData.Nodes[connectionOrder[i]].X * ratio,
                        X2 = _genetic.GTspData.Nodes[connectionOrder[nextIndex]].X * ratio,
                        Y1 = _genetic.GTspData.Nodes[connectionOrder[i]].Y * ratio,
                        Y2 = _genetic.GTspData.Nodes[connectionOrder[nextIndex]].Y * ratio
                    };
                LinkCanvas.Children.Add(line);
            }
        }

        public void _clearConnections()
        {
            LinkCanvas.Children.RemoveRange(0, LinkCanvas.Children.Count);
        }

        #endregion // Graphics Methods

        #region Helper Methods
        private void _populateStatusList(IEnumerable<City> cities)
        {
            StatusListBox.Items.Clear();
            const string header = "id :   X   |   Y   ";
            StatusListBox.Items.Add(header);
            foreach (var n in cities)
            {
                StatusListBox.Items.Add(n.ToString());
            }
        }

        private void _resetSettings()
        {
            PopulationSizeTextBox.Text = AlgorithmSettings.PopulationSize.ToString(CultureInfo.InvariantCulture);
            GroupSizeTextBox.Text = AlgorithmSettings.GroupSize.ToString(CultureInfo.InvariantCulture);
            MutationRateTextBox.Text = AlgorithmSettings.MutationRate.ToString(CultureInfo.InvariantCulture);
            UseClosestTextBox.Text = AlgorithmSettings.UseClosestCityRate.ToString(CultureInfo.InvariantCulture);

            GroupSizeSlider.Maximum = AlgorithmSettings.PopulationSize;
            GroupSizeSlider.Minimum = 5;

            GroupSizeSlider.Value = AlgorithmSettings.GroupSize;
            MutationRateSlider.Value = AlgorithmSettings.MutationRate;
            UseClosestSlider.Value = AlgorithmSettings.UseClosestCityRate;

            if(InitialConnectDropDown.Items.Count <= 0)
                foreach (var m in Enum.GetValues(typeof(Tour.Connection)))
                    InitialConnectDropDown.Items.Add(m.ToString());

            InitialConnectDropDown.SelectedIndex = 0;

            if (InitialConnectDropDown.SelectedIndex == (int)Tour.Connection.ClosestWithRandom)
                _setUseClosestVisibility(Visibility.Visible);
        }

        private void _setUseClosestVisibility(Visibility visibility)
        {
            if (UseClosestLabel != null)
                UseClosestTextBox.Visibility = UseClosestSlider.Visibility = UseClosestLabel.Visibility = visibility;
        }

        private static void _matchTextToSliderValue(TextBox textBox, double newValue)
        {
            textBox.Text = newValue.ToString(CultureInfo.InvariantCulture);
        }
        #endregion // Helper Methods

        #region WPF Events

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            GroupSizeTextBox.IsEnabled = false;
            MutationRateTextBox.IsEnabled = false;
            UseClosestTextBox.IsEnabled = false;
            _setUseClosestVisibility(Visibility.Hidden);

            UseClosestSlider.Maximum = MutationRateSlider.Maximum = 1.0;
            UseClosestSlider.Minimum = MutationRateSlider.Minimum = 0.0;
            UseClosestSlider.TickFrequency = MutationRateSlider.TickFrequency = 0.01;
            UseClosestSlider.IsSnapToTickEnabled = MutationRateSlider.IsSnapToTickEnabled = GroupSizeSlider.IsSnapToTickEnabled = true;

            GroupSizeSlider.TickFrequency = 1;

            ControlProgressBar.Visibility = Visibility.Visible;

            _resetSettings();
        }

        private void InitialConnectDropDown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _setUseClosestVisibility(InitialConnectDropDown.SelectedIndex == 0 ? Visibility.Visible : Visibility.Hidden);
        }

        private void MutationRateSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _matchTextToSliderValue(MutationRateTextBox, e.NewValue);
        }

        private void GroupSizeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _matchTextToSliderValue(GroupSizeTextBox, e.NewValue);
        }

        private void UseClosestSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _matchTextToSliderValue(UseClosestTextBox, e.NewValue);
        }

        #endregion // WPF Events

        #region Button Events

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            _resetSettings();
            _clearConnections();
            _genetic = null;
        }

        private void RunButton_Click(object sender, RoutedEventArgs e)
        {
            if (_genetic == null)
                _createNewGeneticSolver();

            if (_genetic == null) return;

            // Disable buttons (except the stop button)
            RunButton.IsEnabled = false;
            ResetButton.IsEnabled = false;
            StopButton.IsEnabled = true;

            _resetGeneticWorker();

            if (!_geneticWorker.IsBusy)
                _geneticWorker.RunWorkerAsync();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Stop Requested at " + _genetic.Generations + " generations!");
            _geneticWorker.CancelAsync();
            _geneticWorker.Dispose();

            // Enable buttons (except the stop button)
            RunButton.IsEnabled = true;
            ResetButton.IsEnabled = true;
            StopButton.IsEnabled = false;
        }
        #endregion // Button Events

        #region Dialogs

        /// <summary>
        ///     Prompts user to open .tsp file to use for Genetic Algorithm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenMenuClick(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog
                {
                    FileName = "Random100",
                    DefaultExt = ".tsp",
                    Filter = "TSP File (.tsp)|*.tsp"
                };

            var result = dialog.ShowDialog();
            var valid = true;

            if (result == true && dialog.CheckFileExists)
            {
                _tspFileName = dialog.FileName;

                // Check to make sure file selected can be parsed as TSP format
                var tspData = new TspData(_tspFileName);

                if (tspData.ParseSuccess)
                {
                    FilenameLabel.Content = _tspFileName;
                    _resetSettings();
                    DrawCities(tspData.Nodes);
                    _populateStatusList(tspData.Nodes);
                }
                else
                    valid = false;
            }
            else
                valid = false;

            if (!valid)
            {
                FilenameLabel.Content = "Open a TSP File...";
                MessageBox.Show("Unable to open file!");
            }
        }

        #endregion // Dialogs
    }
}