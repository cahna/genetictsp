﻿using GeneticTspSolver.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GeneticTspSolverTests
{
    [TestClass]
    public class ParallelGeneticTspTests
    {
        #region Test Properties
        private const string InFile30 = @"../../Data/Random30.tsp";
        private ParallelGeneticTsp _geneticParallel;
        #endregion // Test Properties

        /// <summary>
        /// Method called before every test to be performed
        /// </summary>
        [TestInitialize]
        public void ParallelGeneticTspSetup()
        {
            _geneticParallel = new ParallelGeneticTsp(InFile30);
        }

        [TestMethod]
        public void TestParallelGeneticTspInit()
        {
            Assert.IsNotNull(_geneticParallel);
            Assert.IsNotNull(GeneticTsp.R);
            Assert.IsNotNull(_geneticParallel.GTspData);
            Assert.IsNotNull(_geneticParallel.GMap);
            Assert.IsNotNull(_geneticParallel.Population);
            Assert.IsNotNull(_geneticParallel.Generations);

            Assert.IsTrue(_geneticParallel.Generations == 0);
            Assert.IsTrue(_geneticParallel.Population.Count == AlgorithmSettings.PopulationSize);

            // Check the population for...
            for (var i = 0; i < _geneticParallel.Population.Count - 2; i++)
            {
                // ...Proper order
                Assert.IsTrue(_geneticParallel.Population[i].RouteDistance <= _geneticParallel.Population[i + 1].RouteDistance);

                // ...Proper size based off static Map attribute set during instantiation of a new GeneticTsp
                Assert.IsTrue(_geneticParallel.Population[i].Route.Count == Map.CityCount);
            }
        }
    }
}
