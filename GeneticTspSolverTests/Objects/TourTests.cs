﻿using GeneticTspSolver.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GeneticTspSolverTests.Objects
{
    /// <summary>
    /// Summary description for MapTests
    /// </summary>
    [TestClass]
    public class TourTests
    {
        #region Test Properties
        // Known values
        private const string InFile30 = @"../../Data/Random30.tsp";

        // Test objects
        private GeneticTsp _genetic;
        private Tour _tour;

        #endregion // Test Properties

        /// <summary>
        /// Method called before every test to be performed
        /// </summary>
        [TestInitialize]
        public void TourSetup()
        {
            _genetic = new GeneticTsp(InFile30);
        }

        [TestMethod]
        public void TestInitInOrder()
        {
            _tour = new Tour(_genetic.GTspData.Nodes.Count);
            Assert.IsTrue(_tour.Route.Count == Map.CityCount);
            Assert.IsNotNull(_tour);
            Assert.IsNotNull(_tour.RouteDistance);
        }

        [TestMethod]
        public void TestInitRandom()
        {
            _tour = new Tour(_genetic.GTspData.Nodes.Count, Tour.Connection.RandomOnly);
            Assert.IsTrue(_tour.Route.Count == Map.CityCount);
            Assert.IsNotNull(_tour);
            Assert.IsNotNull(_tour.RouteDistance);
        }

        [TestMethod]
        public void TestInitClosestWithRandom()
        {
            _tour = new Tour(_genetic.GTspData.Nodes.Count, Tour.Connection.ClosestWithRandom);
            Assert.IsTrue(_tour.Route.Count == Map.CityCount);
            Assert.IsNotNull(_tour);
            Assert.IsNotNull(_tour.RouteDistance);
        }

        [TestMethod]
        public void TestInitClosestCity()
        {
            _tour = new Tour(_genetic.GTspData.Nodes.Count, Tour.Connection.ClosestCity);
            Assert.IsTrue(_tour.Route.Count == Map.CityCount);
            Assert.IsNotNull(_tour);
            Assert.IsNotNull(_tour.RouteDistance);
        }

        [TestMethod]
        public void TestInitGreedyPointToLine()
        {
            _tour = new Tour(_genetic.GTspData.Nodes.Count, Tour.Connection.GreedyPointToLine);
            Assert.IsTrue(_tour.Route.Count == Map.CityCount);
            Assert.IsNotNull(_tour);
            Assert.IsNotNull(_tour.RouteDistance);
        }

        [TestMethod]
        public void TestCreateTourFromRouteList()
        {
            _tour = new Tour(_genetic.GTspData.Nodes.Count);
            var newTour = new Tour(_tour.Route);

            Assert.AreNotSame(_tour, newTour);

            for(var i = 0; i < _tour.Route.Count; i++)
                Assert.IsTrue(_tour.Route[i] == newTour.Route[i]);
        }

        [TestMethod]
        public void TestEquals()
        {
            // Create tours using inorder method (should be indentical routes)
            _tour = new Tour(_genetic.GTspData.Nodes.Count);
            var newTour = new Tour(_genetic.GTspData.Nodes.Count);
            var notEqualTour = new Tour(_genetic.GTspData.Nodes.Count, Tour.Connection.RandomOnly);

            Assert.AreNotSame(_tour, newTour);
            Assert.IsTrue(_tour.Equals(newTour));
            Assert.IsFalse(_tour.Equals(notEqualTour));
        }
    }
}
