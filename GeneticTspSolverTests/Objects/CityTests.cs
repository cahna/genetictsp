﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneticTspSolver.Objects;

namespace GeneticTspSolverTests.Objects
{
    [TestClass]
    public class CityTests
    {
        #region Test Properties
        private City _localCity;
        #endregion // Test Properties

        /// <summary>
        /// Method called before every test to be performed
        /// </summary>
        [TestInitialize]
        public void NodeSetup()
        {
            _localCity = new City(1, 3.0, 4.0);
        }

        [TestMethod]
        public void TestCityInit()
        {
            Assert.IsNotNull(_localCity.Index);
            Assert.IsNotNull(_localCity.X);
            Assert.IsNotNull(_localCity.Y);
        }

        [TestMethod]
        public void TestCreateFromExistingWithoutPreservingConnections()
        {
            var newNode = new City(_localCity);
            Assert.AreNotSame(newNode, _localCity);
        }
    }
}