﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneticTspSolver.Objects;

namespace GeneticTspSolverTests.Objects
{
    [TestClass]
    public class TspDataTests
    {
        #region Test Properties

        private const string InFile100 = @"../../Data/Random100.tsp";
        private const string InFile30 = @"../../Data/Random30.tsp";
        private TspData _localTspData100;
        private TspData _localTspData30;
        #endregion // Test Properties

        /// <summary>
        /// Method called before every test to be performed
        /// </summary>
        [TestInitialize]
        public void TspDataSetup()
        {
            _localTspData100 = new TspData(InFile100);
            _localTspData30 = new TspData(InFile30);
        }

        [TestMethod]
        public void TestTspDataInit()
        {
            Assert.IsNotNull(_localTspData100);
            Assert.IsNotNull(_localTspData100.Nodes);
            Assert.IsTrue(_localTspData100.Nodes.Count == 100);

            Assert.IsNotNull(_localTspData30);
            Assert.IsNotNull(_localTspData30.Nodes);
            Assert.IsTrue(_localTspData30.Nodes.Count == 30);
        }

        [TestMethod]
        public void TestCloneNodeList()
        {
            var newNodesList = TspData.CloneCityList(_localTspData100.Nodes);
            Assert.AreNotSame(newNodesList, _localTspData100.Nodes);
        }
    }
}