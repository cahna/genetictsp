﻿using System;
using System.Collections.Generic;
using GeneticTspSolver.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GeneticTspSolverTests.Objects
{
    /// <summary>
    /// Summary description for MapTests
    /// </summary>
    [TestClass]
    public class MapTests
    {
        #region Test Properties
        private Map _map;
        private int _cityCount;
        private static double Epsilon { get { return 0.001; } }
        #endregion // Test Properties

        /// <summary>
        /// Method called before every test to be performed
        /// </summary>
        [TestInitialize]
        public void MapSetup()
        {
            var c1 = new City(1, 10.0, 10.0);
            var c2 = new City(2, 20.0, 20.0);
            var c3 = new City(3, 30.0, 30.0);
            _cityCount = 3;

            var cityList = new List<City> {c1, c2, c3};
            Assert.IsTrue(cityList.Count == _cityCount);

            _map = new Map(cityList);
        }

        [TestMethod]
        public void TestMapInit()
        {
            Assert.IsNotNull(_map);
            Assert.IsTrue(Map.DistancesBetweenCities.Length == _cityCount * _cityCount);
        }

        [TestMethod]
        public void TestDist()
        {
            Assert.IsTrue(Math.Abs(Map.Dist(0, 0, 0, 0) - 0.0) < Epsilon);
            Assert.IsTrue(Math.Abs(Map.Dist(1, 1, 1, 1) - 0.0) < Epsilon);
            Assert.IsTrue(Math.Abs(Map.Dist(1, 1, 2, 1) - 1) < Epsilon);
        }

        [TestMethod]
        public void TestDistanceBetween()
        {
            var dist1 = Map.Dist(10.0, 10.0, 20.0, 20.0);
            Assert.IsTrue(Math.Abs(Map.DistanceBetweenCities(0, 1) - dist1) < Epsilon);
        }
    }
}
