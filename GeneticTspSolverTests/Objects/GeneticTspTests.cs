﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneticTspSolver.Objects;

namespace GeneticTspSolverTests.Objects
{
    [TestClass]
    public class GeneticTspTests
    {
        #region Test Properties
        private const string InFile30 = @"../../Data/Random30.tsp";
        private GeneticTsp _genetic;
        #endregion // Test Properties

        /// <summary>
        /// Method called before every test to be performed
        /// </summary>
        [TestInitialize]
        public void GeneticTspSetup()
        {
            _genetic = new GeneticTsp(InFile30);
        }

        [TestMethod]
        public void TestGeneticTspInit()
        {
            Assert.IsNotNull(_genetic);
            Assert.IsNotNull(GeneticTsp.R);
            Assert.IsNotNull(_genetic.GTspData);
            Assert.IsNotNull(_genetic.GMap);
            Assert.IsNotNull(_genetic.Population);
            Assert.IsNotNull(_genetic.Generations);

            Assert.IsTrue(_genetic.Generations == 0);
            Assert.IsTrue(_genetic.Population.Count == AlgorithmSettings.PopulationSize);

            // Check the population for...
            for (var i = 0; i < _genetic.Population.Count - 2; i++)
            {
                // ...Proper order
                Assert.IsTrue(_genetic.Population[i].RouteDistance <= _genetic.Population[i + 1].RouteDistance);

                // ...Proper size based off static Map attribute set during instantiation of a new GeneticTsp
                Assert.IsTrue(_genetic.Population[i].Route.Count == Map.CityCount);
            }
        }
    }
}